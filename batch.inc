<?php

/**
 * @file
 * Batch Operations.
 */

/**
 * Process to load the node fields.
 */
function find_external_links_process_1($nid, $ignore_strings, $field_list, $num_operations, &$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox'] = [];
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    // Save node count for the termination message.
    $context['sandbox']['max'] = $num_operations;
  }
  $node = \Drupal::service('entity_type.manager')->getStorage('node')->load($nid);
  // For each node, loop through the possible fields.
  foreach ($field_list as $field) {
    // If the field exists and is not empty.
    if ($node->hasField($field) /*&& !empty($node->get($field)->value)*/) {
      // Determine the bundle.
      $entityObj = \Drupal::service('entity_type.manager')->getStorage('node')->load($nid);
      $bundle = $entityObj->bundle();

      // Call the function that searches for links and logs them to the db.
      find_external_links_url($node->get($field)->value, $nid, $bundle, $ignore_strings);

    }
  }
  $context['results'][] = $nid;
  $context['sandbox']['current_node'] = $nid;
  $context['sandbox']['progress']++;

}

/**
 * Process to update links pointing to external sites.
 *
 *  Node content with find external links.
 */
function find_external_links_url($body_data, $nid, $bundle, &$ignore_strings) {
  // If urls start with these, then they are not external links.
  $local_start = ['#', '/'];

  if (!empty($body_data)) {
    $doc = new DOMDocument();
    $content_html = mb_convert_encoding($body_data, 'HTML-ENTITIES', "UTF-8");
    $doc->loadHTML($content_html);
    foreach ($doc->getElementsByTagName('a') as $link) {
      $url = $link->getAttribute('href');
      // Default to adding the url.
      $ignore = FALSE;

      // If url starts with $local_start (it's an internal link)
      if (in_array($url[0], $local_start)) {
        // It is a local link, so le'ts ignore.
        $ignore = TRUE;
      }
      // There is no href or it's blank.
      elseif (is_null($url) || $url == '') {
        $ignore = TRUE;
      }
      // it's not a local link or blank, so let's see if we should ignore it.
      else {
        // But it might match an ignore_string, so check against those now.
        if (!$ignore_strings) {
          foreach ($ignore_strings as $ignore_string) {
            if (stripos($url, trim($ignore_string)) !== FALSE) {
              $ignore = TRUE;
              continue;
            }
          }
        }
      }
      // If it makes it here with ignore still false, add the url to the list.
      if (!$ignore) {
        $external_link_entry = [
          'content_type' => $bundle,
          'url' => $url,
          'nid' => $nid,
        ];
        \Drupal::database()->insert('find_external_links')
          ->fields($external_link_entry)
          ->execute();
      }
    }
  }
}

/**
 * Batch process finish function for finding external urls.
 */
function find_external_links_finished($success, $results, $operations) {
  if ($success) {
    \Drupal::messenger()->addStatus(t('You can see the updated "External links list" tab now. All previously found links were removed before generating the new list!!.'));

  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $operation_array = [
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    ];
    \Drupal::messenger()->addStatus(t('An error occurred while processing @operation with arguments : @args', $operation_array));
  }
  return TRUE;
}
