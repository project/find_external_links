<?php

namespace Drupal\find_external_links\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form constructor for find_external_links_input_form.
 */
class ConfigurationForm extends FormBase {

  /**
   * Drupal connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * Constructs a new RouterRebuildConfirmForm object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The list of available modules.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ModuleExtensionList $extension_list_module) {
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->extensionListModule = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'find_external_links.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'find_external_links_input_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('find_external_links.settings');

    $content_types = node_type_get_names();
    $field_list = $this->getFieldList();

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Select the options below and submit to have the "External links list" tab re-created.</p>'),
    ];
    $form['content_types'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Content types to search'),
      '#description'   => $this->t('Please select content type you want to find for external links.'),
      '#default_value' => ($config->get('content_types')) ? $config->get('content_types') : [],
      '#options'       => $content_types,
      '#multiple'      => TRUE,
    ];
    $form['ignore_strings'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Ignore strings'),
      '#default_value' => ($config->get('ignore_strings')) ? $config->get('ignore_strings') : [],
      '#description'   => $this->t('One per line. URLs found with these strings will be ignored and not added to the list of external links. Could be a domain or any part of the href'),
    ];
    $form['field_list'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Fields to scan'),
      '#description'   => $this->t('<p>mark the fields you would like this scanner to look for links in</p>'),
      '#default_value' => ($config->get('field_list')) ? $config->get('field_list') : [],
      '#multiple'      => TRUE,
      '#options'       => $field_list,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Find External Links'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate that at least one content type is selected.
    $content_types = $this->getSelectedContentTypes($form_state->getValue('content_types'));

    if (empty($content_types)) {
      $form_state->setErrorByName('content_types', $this->t('Seems you have not selected any content type. Please select at least one content type you want to search first.'));
    }
    // Validate that at least one field is selected.
    $field_list = $this->getSelectedFieldList($form_state->getValue('field_list'));
    if (empty($field_list)) {
      $form_state->setErrorByName('field_list', $this->t('Seems you have not selected any fields. Please select at least one field you want to search first.'));

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update the settings to save what was selected.
    $this->configFactory->getEditable('find_external_links.settings')
      ->set('content_types', $form_state->getValue('content_types'))
      ->set('ignore_strings', $form_state->getValue('ignore_strings'))
      ->set('field_list', $form_state->getValue('field_list'))
      ->save();

    // Array of machine name content_types selected.
    $content_types = $this->getSelectedContentTypes($form_state->getValue('content_types'));

    // Clear out list of external links.
    $this->connection->delete('find_external_links')
      ->execute();

    $nids = $this->findExternalLinksSelectedNid($content_types);

    if (!count($nids)) {
      $this->messenger()->addStatus($this->t("Seems You don't currently have any nodes with the selected content types."));
    }
    // Execute the function named update_external_links_batch_start.
    else {
      // Pass in the strings to ignore when looping through links.
      $ignore_strings = explode(PHP_EOL, $form_state->getValue('ignore_strings'));
      // Pass in what fields to look through.
      $field_list = $this->getSelectedFieldList($form_state->getValue('field_list'));

      $batch = $this->findExternalLinksBatchStart($nids, $ignore_strings, $field_list);

      batch_set($batch);
    }
  }

  /**
   * Utility function- simply querie and loads all nid of selected content type.
   */
  public function findExternalLinksSelectedNid(array $content_types_name) {
    $query = $this->connection->select('node', 'n');
    $query->fields('n', ['nid'])->condition('n.type', $content_types_name, 'IN')->orderBy('n.nid', 'ASC');
    $nids = $query->execute()->fetchCol();
    return $nids;

  }

  /**
   * Batch process start function.
   */
  public function findExternalLinksBatchStart($node_ids, $ignore_strings, $field_list) {
    $num_operations = count($node_ids);

    if ($num_operations > 0) {
      $operations = [];
      for ($i = 0; $i < $num_operations; $i++) {
        $nid = $node_ids[$i];
        $operations[] = [
          'find_external_links_process_1',
          [
            $nid,
            $ignore_strings,
            $field_list,

            $this->t('(Operation @operation)', ['@operation' => $num_operations]),
          ],
        ];
      }
    }

    $batch = [
      'operations' => $operations,
      'finished' => 'find_external_links_finished',
      'title' => $this->t('Processing External Links'),
      'init_message' => $this->t('Find external links process is starting.'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('Find External Link has encountered an error.'),
      'file' => $this->extensionListModule->getPath('find_external_links') . '/batch.inc',
    ];
    return $batch;
  }

  /**
   * Re-used code that returns an array of selected content_type machine names.
   */
  public function getSelectedContentTypes(array &$selected_content_types) {
    $content_types = [];
    $available_content_types = node_type_get_names();

    foreach ($selected_content_types as $value) {
      if ($value != '0' && array_key_exists($value, $available_content_types)) {
        $content_types[] = $value;
      }
    }

    return $content_types;
  }

  /**
   * Purpose: returns a machine name array of selected fields.
   */
  public function getSelectedFieldList(array &$selected_field_list) {
    $field_list = [];
    $available_field_list = $this->getFieldList();
    foreach ($selected_field_list as $key => $value) {
      if ($value != '0' && array_key_exists($key, $available_field_list)) {
        $field_list[] = $key;
      }
    }

    return $field_list;
  }

  /**
   * Purpose: generate a list of all fields used in the various content types.
   */
  private function getFieldList() {

    $field_list = [];
    // Get node types.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // For each content type, get the text_long fields for that content type.
    foreach (array_keys($node_types) as $key => $value) {

      $fields = $this->entityFieldManager->getFieldDefinitions("node", $value);
      foreach ($fields as $key => $value) {
        if (!in_array($key, $field_list)) {
          if (strpos($value->getType(), 'text') !== FALSE) {
            $field_list[$key] = $key;
          }
        }
      }
    }

    asort($field_list);

    if (is_null($field_list)) {
      return [];
    }
    else {
      return $field_list;
    }
  }

}
