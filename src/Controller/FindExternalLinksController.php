<?php

namespace Drupal\find_external_links\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Controller routines for find_external_links.
 *
 * @ingroup find_external_links
 */
class FindExternalLinksController extends ControllerBase {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */

  private $connection;

  /**
   * ServiceInfoController constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Implements display External Links list.
   */
  public function displayExternalLinks() {
    $header = [
      'content_type' => $this->t('Content Type'),
      'url' => $this->t('Url'),
      'nid' => $this->t('Nid'),
    ];
    $query = $this->connection->select('find_external_links', 'fel');
    $query->fields('fel', []);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(25);

    $results = $pager->execute()->fetchAll();

    // Initiate variable in case there are no results.
    $rows = [];

    foreach ($results as $value) {
      $rows[] = [
        'content_type' => $value->content_type,
        'url' => $value->url,
        'nid' => new FormattableMarkup('<a href="/node/@nid" target="_blank">@nid</a>', ['@nid' => $value->nid]),
      ];
    }

    // Display data in site.
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No data found'),
    ];

    // Finally add the pager.
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

}
