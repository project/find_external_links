# Find external links

- This module find the external URLs list presents in the body content.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

This module find the external URLs list presents in the body content.

- [For a full description of the module, visit the project page:]
  (<https://www.drupal.org/project/find_external_links/>)

- [To submit bug reports and feature suggestions, or to track changes:]
  (<https://www.drupal.org/project/issues/search/find_external_links/>)

## REQUIREMENTS

- This module requires no modules outside of Drupal core.

## INSTALLATION

- Install as you would normally install a contributed Drupal module.
  [See:](https://www.drupal.org/node/895232) for further information.

## Configuration

- Enable the Find external links module.
- Go to "/admin/config/system/find-external-links".
- Select the required options and save.

## Maintainers

- Amrendra Mourya - [amrendra](https://www.drupal.org/u/amrendra)
- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- Pappu Kumar Singh - [pappuksingh](https://www.drupal.org/u/pappuksingh)
